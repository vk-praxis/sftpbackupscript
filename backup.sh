#!/bin/bash
# A simple shell script To upload backups to an sftp server and send an email notification
# Fapsi - 29/04/2019

# include parse_yaml function
. ./parse_yaml.sh

# read yaml file
eval $(parse_yaml config.yml "config_")

# local variables
config_currentdir=`pwd`
config_sftp_commandfile="$config_currentdir"/sftp-put.txt
config_sftp_commandfiletmp="$config_currentdir"/sftp-put.tmp.txt

mount -t cifs -o username=$config_smbshare_user,password=$config_smbshare_password,domain=$config_smbshare_domain,ro,vers=1.0 $config_smbshare_remoteshare $config_backup_localdir 

# display variables
echo "$config_currentdir"

echo "backup_localdir: $config_backup_localdir"
echo "backup_remotedir: $config_backup_remotedir"
echo "backup_minsize: $config_backup_minsize"

echo "sftp_host: $config_sftp_host"
echo "sftp_port: $config_sftp_port"
echo "sftp_username: $config_sftp_username"
echo "sftp_keyfile: $config_sftp_keyfile"
echo "sftp.commandfile: $config_sftp_commandfile"
echo "sftp.commandfiletmp: $config_sftp_commandfiletmp"

echo "email_to: $config_email_to"
echo "email_cc: $config_email_cc"
echo "email_subject_ok: $config_email_subject_ok"
echo "email_subject_err: $config_email_subject_err"

if [ -d $config_backup_localdir ] 
  then
  SIZE=$(du -sb $config_backup_localdir | cut -f1) 
  echo $SIZE
  if [ $SIZE -gt $config_backup_minsize ]
  then
    sed "s|template|$config_backup_localdir|g" <$config_sftp_commandfile >$config_sftp_commandfiletmp
    sftp -P $config_sftp_port -oConnectTimeout=10 -oBatchMode=yes -i $config_sftp_keyfile -b $config_sftp_commandfiletmp -v "$config_sftp_username@$config_sftp_host:$config_backup_remotedir" |& tee output.txt
    RESULT=${PIPESTATUS[0]}
    echo "RESULT: $RESULT"
    if [ $RESULT -eq 0 ]; then
      cat output.txt | mail -s "$config_email_subject_ok" -c $config_email_cc $config_email_to
    else
      cat output.txt | mail -s "$config_email_subject_err" -c $config_email_cc $config_email_to
      exit 1
    fi
  else
    echo "ERROR: Almost Empty folder, stopping upload! Size: $SIZE Min-Size:$config_backup_minsize" | mail -s "$config_email_subject_err" -c $config_email_cc $config_email_to
    exit 1
  fi
else
  echo "ERROR: Directory not exists, stopping upload!" | mail -s "$config_email_subject_err" -c $config_email_cc $config_email_to
  exit 1
fi
